# Sentiment Analysis Workshop ---------------------------------------------

#PRE-FLIGHT 

#Load Libraries 
library(readr)
library(dplyr)
library(tidyr)
library(tidytext)
library(ggplot2)

#Load Data
data <- read_csv("datasets/gop_debates.csv")


# Explore Sentiment Dictionaries ------------------------------------------
afin <- get_sentiments("afinn")
bing <- get_sentiments("bing")
nrc <- get_sentiments("nrc")


# Tokenize By Word --------------------------------------------------------
data %>% 
  unnest_tokens(words, text, token = "words")

data$id <- row.names(data)

data_tokens <- data %>% 
  unnest_tokens(word, text, token = "words")


# Sentiment Analysis with inner join --------------------------------------
data_tokens %>% 
  inner_join(afin) %>% 

data_tokens %>% 
  inner_join(bing)

data_tokens %>% 
  inner_join(nrc)


# Basic Sentiment Math ----------------------------------------------------
# View Talking Turn 3 DFM | bing
data_tokens %>% 
  filter(id==3) %>% 
  inner_join(bing) 

# View Talking Turn 3 DFM | afin
data_tokens %>% 
  filter(id==3) %>% 
  inner_join(afin) 

# Get Talking Turn 3 Sentiment Score | afin
data_tokens %>% 
  filter(id==3) %>% 
  inner_join(afin) %>% 
  select(value) %>% 
  colSums()

# View Talking Turn 10 DFM | bing
data_tokens %>% 
  filter(id==10) %>% 
  inner_join(bing) 

# View Talking Turn 10 DFM | afin
data_tokens %>% 
  filter(id==10) %>% 
  inner_join(afin) 

# Get Talking Turn 10 Sentiment Score | afin
data_tokens %>% 
  filter(id==10) %>% 
  inner_join(afin) %>% 
  select(value) %>% 
  colSums()


# Analyses by Speaker ------------------------------------------------------
data_tokens %>% 
  inner_join(afin) %>% 
  group_by(who) %>% 
  summarise(sent = mean(value))

data_tokens %>% 
  inner_join(nrc) %>% 
  filter(sentiment=="fear") %>% 
  count(who) %>% 
  arrange(desc(n))


# Your Turn ---------------------------------------------------------------

# Using the Bing dictionary, find out which candidate has the greatest number of negative words 
# Plot your results 

# Using the NRC dictionary, find out which candidate has the greatest number of trust words. 
# Plot your results 

# Using the afin dictionary, plot the average sentiment score per debate over time. 


