# Stackoverflow Scavenger Hunt  -------------------------------------------


# 1. Create a new RStudio project in a new working directory   -------------
# URL: 


# 2. Load both your (HW#1) & the provdied Longhorn tweets as dataframes.  --

# URL: 



# 3 Combine your data with my data ----------------------------------------

# STEP 3A

# URL:


# STEP 3B

# URL:


# STEP 3C

# URL:



# 4. Create a new column that adds the retweet and favorite count together ---

# URL: 



# 5. Create a new column that flags each row as mine or yours.  -----------
# Hint: Create the new column based on the date column.  

# URL:



# 6. Create a list with all the tweet text. -------------------------------

# URL:



# 7. Create a list object with ratings for each tweet.   ------------------
# Rate tweets 1-5 with 1 = not cool and  5 = very cool. 

# URL:



# 8. Add the rating list to the DF as a column.  --------------------------

# URL:



# 9. Save your final DF as a .csv -----------------------------------------

# URL:






